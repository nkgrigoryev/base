import '@casino/demo-css';
import './css/styles.css';
import I18n from '@casino/i18n';
import {Base, BaseView, beautifyNumber} from '../';
import createElementFromHTML from "@casino/create-element-from-html";

function demo() {
    class DemoView extends BaseView {
        constructor() {
            super();
            this.create(`<div class="demo" data-i18n="helloWorld">x</div>`);
        }

        destroy() {
            this.remove();
        }
    }

    let translations = {
        en: {
            helloWorld: 'Hello world!'
        },
        ru: {
            helloWorld: 'Привет мир!'
        }
    };

    let i18n = new I18n(translations);
    Base.i18n = i18n;

    let demo = new DemoView();
    document.getElementById('demo-js').appendChild(demo.element);

    document.getElementById('en-js').addEventListener('click', () => {
        i18n.changeLanguage('en');
    });
    document.getElementById('ru-js').addEventListener('click', () => {
        i18n.changeLanguage('ru');
    });
    document.getElementById('remove-js').addEventListener('click', () => {
        demo.destroy();
    });
    document.getElementById('create-js').addEventListener('click', () => {
        demo = new DemoView();
        document.getElementById('demo-js').appendChild(demo.element);
    });
}

function beautifyNumberDemo() {
    let numbers = [
        0.1,
        0.01,
        0.001,
        0.0001,
        0.00001,
        0.000012,
        0.0000123,
        0.00001235,
        0.000012356,
        123456789,
        12345678.9,
        1234567.89,
        123456.789,
        12345.6789,
        1234.56789,
        123.456789,
        12.3456789,
        1.23456789,
    ];
    for (let i = 0; i < numbers.length; i++) {
        let number = numbers[i];
        let beautyNumber = beautifyNumber(number);
        let html = `<p>${number}: ${beautyNumber}</p>`;
        let element = createElementFromHTML(html);
        document.getElementById('beautify-number-js').appendChild(element);
    }
}

demo();
beautifyNumberDemo();