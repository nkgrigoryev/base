import Base from "./src/Base";
import BaseView from "./src/BaseView";
import BaseViewEventDispatcher from "./src/BaseViewEventDispatcher";
import beautifyNumber from './src/utils/beautifyNumber';

export {BaseView as default};
export {Base, BaseView, BaseViewEventDispatcher, beautifyNumber};