import createElementFromHTML from '@casino/create-element-from-html';
import escapeHTML from '@casino/escape-html';
import EventDispatcher from '@casino/event-dispatcher';
import Base from './Base';

export default class BaseViewEventDispatcher extends EventDispatcher {
    protected _element: HTMLElement;

    public get element(): HTMLElement {
        return this._element;
    }

    protected create(html: string): void {
        this._element = createElementFromHTML(html);
        BaseViewEventDispatcher.t(this._element);
    }

    protected remove(): void {
        this._element.remove();
        this._element = null;
        BaseViewEventDispatcher.tRemove(this._element);
    }

    protected static t(element: Element, parameters: Object = {}): void {
        if (Base.i18n) Base.i18n.t(element, parameters)
    }

    protected static tRemove(element: Element): void {
        if (Base.i18n) Base.i18n.remove(element)
    }

    static escape(string: string): string {
        return escapeHTML(string);
    }
}