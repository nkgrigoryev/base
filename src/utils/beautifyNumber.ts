let suffixesBig = ['', 'k', 'M', 'G', 'T', 'P', 'E'];
let suffixesSmall = ['', 'm', 'µ', 'n', 'p'];
let BIG = 'BIG';
let SMALL = 'SMALL';
const MINUS = '-';
const EMPTY = '';

/**
 * @param number {Number} Example:
 *     123456
 *     1234
 *     0.01
 *     0.001234
 *     1234
 * @param digits {Number} Example:
 *     4
 * @return {String} Example:
 *     123.4k
 *     1234
 *     0.01
 *     1.234m
 */
export default function beautifyNumber(number: number, digits: number = 4): string {
    let prefix = number < 0 ? MINUS : EMPTY;
    let numberString = number.toString();
    let hasDot = numberString.indexOf('.') !== -1;
    let numberLength = numberString.length;
    if (hasDot) numberLength--;
    if (numberLength <= digits) return numberString;

    number = Math.abs(number);
    let suffixIndex = 0;
    let suffixes;
    if (number > 1) {
        suffixes = suffixesBig;
        while (number >= 1000) {
            number = number / 1000;
            suffixIndex++;
        }
        if (suffixIndex >= suffixes.length) return SMALL;
    }
    if (number < 1) {
        suffixes = suffixesSmall;
        while (number < 1) {
            number = number * 1000;
            suffixIndex++;
        }
        if (suffixIndex >= suffixes.length) return BIG;
    }
    let suffix = suffixes[suffixIndex];
    let shortNumber = parseFloat(number.toString().substr(0, digits + 1)).toLocaleString();
    return `${prefix}${shortNumber}${suffix}`;
}