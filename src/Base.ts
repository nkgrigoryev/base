import I18n from '@casino/i18n';

export default class Base {
    private static _i18n: I18n;

    public static set i18n(value: I18n) {
        if (Base._i18n) return;
        Base._i18n = value;
    }

    public static get i18n(): I18n {
        return Base._i18n;
    }
}